<?php
/**
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'beonixem_wp456');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '89Yr|ManBV>iBam*d0=L*sjMLY,U7-jx>dD?,I|Q.]&+n=CatXcnFAL;_FCTJ#7n');
define('SECURE_AUTH_KEY',  'ZqLF&L]eo,&PIFtr+j|kVERaj1y-xT}EOxBk1&nkxTT(3!*pgLY|^vMi<DC49Bi-');
define('LOGGED_IN_KEY',    '1l.P,_W(lL>m2SgSzMG)+U$J~Pd;/LD@RbH/B#xaW,&A%}vawoC77?s?{|4$HCxA');
define('NONCE_KEY',        'Px+Qk-4m7J99Ikn7`S><7  lc&?&gZKuJsZoF?|`Ge mp;g_~SFf.R{]3QP<nZ(a');
define('AUTH_SALT',        'A+.wV&ZgTUz:h8Eeg7N/8Nom_ge[)#&U|!V#S|hK(1H704DZJ)2LG%7Fc@&ZOYW0');
define('SECURE_AUTH_SALT', 'aWmf4w+1h19H&/h6:Bs-6w7rfh}]W&#^pz|JOrwi5_aMIz{-RN`}XZ6a%QQBS.]v');
define('LOGGED_IN_SALT',   'p-/,z`:xl2zs)^?^e(;ee>h&B>*6{NT+hk[{:v4Ao3JX0Jnmb|oOB}$+jGqv5ku-');
define('NONCE_SALT',       '&1.`l]@pe1SmGG[S2d>Rxghh]5CeF9Ji#|yInG:c4|5>F{p?8XSGbW)dpY&s24Q0');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wpsr_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', true);

define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] );
define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] );

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

