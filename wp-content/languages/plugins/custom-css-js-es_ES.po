# Translation of Plugins - Simple Custom CSS and JS - Stable (latest release) in Spanish (Spain)
# This file is distributed under the same license as the Plugins - Simple Custom CSS and JS - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2019-11-05 13:57:32+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: GlotPress/2.4.0-alpha\n"
"Language: es\n"
"Project-Id-Version: Plugins - Simple Custom CSS and JS - Stable (latest release)\n"

#: includes/admin-config.php:215
msgid "In your page's HTML there is a comment added before and after the internal CSS or JS in order to help you locate your custom code. Enable this option in order to remove that comment."
msgstr "En el código HTML de tu página hay un comentario añadido antes y después del código CSS o JS para ayudarte a localizar tu código personalizado. Activa esta opción para eliminar ese comentario."

#: includes/admin-config.php:251
msgid "Remove the comments from HTML"
msgstr "Eliminar los comentarios del código HTML"

#: includes/admin-addons.php:83
msgid "Homepage"
msgstr "Página de inicio"

#: includes/admin-config.php:213
msgid "If you use HTML tags in your code (for example "
msgstr "Si usas etiquetas HTML en tu código (por ejemplo"

#: includes/admin-config.php:211
msgid "If you want to use an HTML entity in your code (for example "
msgstr "Si quieres usar una entidad HTML en tu código (por ejemplo"

#: custom-css-js.php:54 custom-css-js.php:61
msgid "An error has occurred. Please reload the page and try again."
msgstr "Ha ocurrido un error. Recarga la página y intentalo de nuevo por favor."

#: includes/admin-config.php:239
msgid "By default only the Administrator will be able to publish/edit/delete Custom Codes. By enabling this option there is also a \"Web Designer\" role created which can be assigned to a non-admin user in order to publish/edit/delete Custom Codes."
msgstr "Por defecto, solo el administrador podrá publicar/editar/borrar códigos personalizados. Activando esta opción también hay un perfil de «Diseñador web» que puede asignarse a un usuario que no sea administrador para que publique/edite/borre códigos personalizados."

#: includes/admin-config.php:243
msgid "Add the \"Web Designer\" role"
msgstr "Añadir perfil de «diseñador web»"

#: includes/admin-screens.php:270
msgid "Published"
msgstr "Publicado"

#. Author of the plugin
msgid "SilkyPress.com"
msgstr "SilkyPress.com"

#: includes/admin-config.php:240
msgid "General Settings"
msgstr "Ajustes generales"

#: includes/admin-screens.php:288 includes/admin-screens.php:291
msgid "Y/m/d g:i:s a"
msgstr "d/m/Y G:i:s"

#: includes/admin-config.php:228
msgid "Encode the HTML entities"
msgstr "Cifra las entidades HTML"

#: includes/admin-config.php:219
msgid "Editor Settings"
msgstr "Ajustes de edición "

#: includes/admin-config.php:261
msgid "Save"
msgstr "Guardar"

#: includes/admin-config.php:152
msgid "Custom CSS & JS Settings"
msgstr "Ajustes personalizados de CSS y JS"

#: includes/admin-config.php:222
msgid "Keep the HTML entities, don't convert to its character"
msgstr "Mantiene las entidades HTML, no las convierte a su carácter"

#: includes/admin-screens.php:375
msgid "CSS Codes"
msgstr "Códigos CSS"

#: includes/admin-screens.php:376
msgid "JS Codes"
msgstr "Códigos JS"

#: includes/admin-screens.php:377
msgid "HTML Codes"
msgstr "Códigos HTML"

#: includes/admin-screens.php:380
msgid "Filter Code Type"
msgstr "Tipo de código de filtrado"

#: includes/admin-screens.php:382
msgid "All Custom Codes"
msgstr "Todos los códigos personalizados"

#: includes/admin-screens.php:679
msgid "Last edited by %1$s on %2$s at %3$s"
msgstr "Ultima edición el %1$s de %2$s en %3$s"

#: includes/admin-screens.php:681
msgid "Last edited on %1$s at %2$s"
msgstr "Última edición el %1$s en %2$s"

#: includes/admin-screens.php:462
msgid "Edit HTML Code"
msgstr "Editar código HTML"

#: includes/admin-screens.php:461
msgid "Edit JS Code"
msgstr "Editar código JS"

#: includes/admin-screens.php:460
msgid "Edit CSS Code"
msgstr "Editar código CSS"

#: includes/admin-screens.php:599
msgid ""
"/* Add your JavaScript code here.\n"
"\n"
"If you are using the jQuery library, then don't forget to wrap your code inside jQuery.ready() as follows:\n"
"\n"
"jQuery(document).ready(function( $ ){\n"
"    // Your code in here\n"
"});\n"
"\n"
"--\n"
"\n"
"If you want to link a JavaScript file that resides on another server (similar to\n"
"<script src=\"https://example.com/your-js-file.js\"></script>), then please use\n"
"the \"Add HTML Code\" page, as this is a HTML code that links a JavaScript file.\n"
"\n"
"End of comment */ "
msgstr ""
"/* Añade aquí tu código JavaScript.\n"
"\n"
"Si estás usando la biblioteca jQuery, entonces no olvides envolver tu código dentro de jQuery.ready() así:\n"
"\n"
"jQuery(document).ready(function( $ ){\n"
"    // Tu código aquí dentro\n"
"});\n"
"\n"
"--\n"
"\n"
"Si quieres enlazar a un archivo JavaScript que resida en otro servidor (como\n"
"<script src=\"https://example.com/your-js-file.js\"></script>), entonces, por favor, usa\n"
"la página «Añadir código HTML» , ya que es un código HTML que enlaza a un archivo JavaScript.\n"
"\n"
"Fin del comentario */ "

#: includes/admin-screens.php:180 includes/admin-screens.php:1242
#: includes/admin-screens.php:1270
msgid "Activate"
msgstr "Activar"

#: includes/admin-screens.php:181 includes/admin-screens.php:1239
#: includes/admin-screens.php:1267
msgid "Deactivate"
msgstr "Desactivar"

#: includes/admin-screens.php:179 includes/admin-screens.php:1269
msgid "Inactive"
msgstr "Inactivo"

#: includes/admin-screens.php:821
msgid "On Login Page"
msgstr "En la página de identificación"

#: includes/admin-addons.php:88
msgid "Starts with"
msgstr "Empieza con"

#: includes/admin-addons.php:87
msgid "Not equal to"
msgstr "No es igual a"

#: includes/admin-addons.php:86
msgid "Is equal to"
msgstr "Es igual a"

#: includes/admin-install.php:34
msgid "New Custom Code"
msgstr "Nuevo código personalizado"

#: includes/admin-install.php:33
msgid "Add Custom Code"
msgstr "Añadir código personalizado"

#: includes/admin-install.php:32
msgctxt "add new"
msgid "Add Custom Code"
msgstr "Añadir código personalizado"

#: includes/admin-screens.php:925
msgid "Both"
msgstr "Ambos"

#: includes/admin-screens.php:802 includes/admin-screens.php:890
msgid "Footer"
msgstr "Pie de página"

#: includes/admin-screens.php:1098
msgid "The %s directory is not writable, therefore the CSS and JS files cannot be saved."
msgstr "El %s directorio no tiene permisos de escritura por lo tanto los archivos de CSS y JS no pueden ser guardados."

#: includes/admin-screens.php:1099
msgid "Please run the following command to make the directory writable"
msgstr "Ejecuta el siguiente comando para condecer permisos de escritura al directorio, por favor"

#: includes/admin-screens.php:1089
msgid "The %s directory could not be created"
msgstr "El directorio %s no pudo ser creado"

#. Plugin URI of the plugin
msgid "https://wordpress.org/plugins/custom-css-js/"
msgstr "https://wordpress.org/plugins/custom-css-js/"

#: includes/admin-screens.php:921
msgid "Mobile"
msgstr "Móvil "

#. Author URI of the plugin
msgid "https://www.silkypress.com"
msgstr "https://www.silkypress.com"

#: includes/admin-screens.php:917
msgid "Desktop"
msgstr "Escritorio"

#: includes/admin-screens.php:851 includes/admin-screens.php:932
msgid "Priority"
msgstr "Prioridad"

#: includes/admin-screens.php:911
msgid "On which device"
msgstr "En qué dispositivo"

#: includes/admin-screens.php:1090
msgid "Please run the following commands in order to make the directory"
msgstr "Ejecuta los siguientes comandos para crear el directorio"

#: includes/admin-screens.php:844
msgid "Minify the code"
msgstr "Minimiza el código"

#: includes/admin-screens.php:827
msgid "CSS Preprocessor"
msgstr "Pre-procesador CSS"

#: includes/admin-screens.php:835
msgid "Less"
msgstr "Menos"

#: includes/admin-screens.php:838
msgid "SASS (only SCSS syntax)"
msgstr "SASS (usa sólo sintaxis SCSS) "

#: includes/admin-screens.php:817 includes/admin-screens.php:905
msgid "In Admin"
msgstr "En administración"

#: includes/admin-screens.php:813 includes/admin-screens.php:901
msgid "In Frontend"
msgstr "En portada"

#: includes/admin-screens.php:798 includes/admin-screens.php:886
msgid "Header"
msgstr "Cabecera"

#: includes/admin-screens.php:832
msgid "None"
msgstr "Ninguno"

#: includes/admin-screens.php:783
msgid "External File"
msgstr "Archivo externo"

#: includes/admin-screens.php:787
msgid "Internal"
msgstr "Interno"

#: includes/admin-screens.php:856 includes/admin-screens.php:937
msgctxt "1 is the highest priority"
msgid "1 (highest)"
msgstr "1 (más alto)"

#: includes/admin-screens.php:865 includes/admin-screens.php:946
msgctxt "10 is the lowest priority"
msgid "10 (lowest)"
msgstr "10 (más bajo)"

#: includes/admin-screens.php:493
msgid "Code updated"
msgstr "Código actualizado"

#: includes/admin-screens.php:440 includes/admin-screens.php:459
#: includes/admin-screens.php:480
msgid "Add HTML Code"
msgstr "Añade código HTML"

#: includes/admin-screens.php:439 includes/admin-screens.php:458
#: includes/admin-screens.php:479
msgid "Add JS Code"
msgstr "Añade código JS"

#: includes/admin-screens.php:437
msgid "Custom Code"
msgstr "Código personalizado"

#: includes/admin-screens.php:438 includes/admin-screens.php:457
#: includes/admin-screens.php:478
msgid "Add CSS Code"
msgstr "Añadir código CSS"

#: includes/admin-screens.php:182 includes/admin-screens.php:309
#: includes/admin-screens.php:1238
msgid "The code is active. Click to deactivate it"
msgstr "El código está activo. Haz clic para desactivarlo"

#: includes/admin-screens.php:183 includes/admin-screens.php:312
#: includes/admin-screens.php:1241
msgid "The code is inactive. Click to activate it"
msgstr "El código está inactivo. Haz clic aquí para activarlo."

#: includes/admin-screens.php:778
msgid "Linking type"
msgstr "Tipo de enlazado"

#. Description of the plugin
msgid "Easily add Custom CSS or JS to your website with an awesome editor."
msgstr "Añade fácilmente código CSS o JS a tu web con un fantástico editor"

#: includes/admin-screens.php:271
msgid "Modified"
msgstr "Modificado"

#: includes/admin-screens.php:268
msgid "Title"
msgstr "Título"

#. Plugin Name of the plugin
msgid "Simple Custom CSS and JS"
msgstr "Simple Custom CSS and JS"

#: includes/admin-screens.php:298
msgid "%s ago"
msgstr "hace %s"

#: includes/admin-screens.php:267
msgid "Type"
msgstr "Tipo"

#: includes/admin-screens.php:178 includes/admin-screens.php:266
#: includes/admin-screens.php:1266
msgid "Active"
msgstr "Activo"

#: includes/admin-screens.php:102
msgid "Add Custom HTML"
msgstr "Añade HTML personalizado"

#: includes/admin-screens.php:190
msgid "Options"
msgstr "Opciones"

#: includes/admin-screens.php:99
msgid "Add Custom JS"
msgstr "Añade JS personalizado"

#: includes/admin-screens.php:252
msgid "Add HTML code"
msgstr "Añade código HTML"

#: includes/admin-screens.php:251
msgid "Add JS code"
msgstr "Añade código JS"

#: includes/admin-screens.php:250
msgid "Add CSS code"
msgstr "Añade código CSS"

#: includes/admin-screens.php:96
msgid "Add Custom CSS"
msgstr "Añade CSS personalizado"

#: includes/admin-notices.php:125 includes/admin-notices.php:190
msgid "Dismiss this notice"
msgstr "Descartar este aviso"

#: includes/admin-notices.php:125
msgid "Get your discount now"
msgstr "Obtén ahora tu descuento"

#: includes/admin-addons.php:140 includes/admin-addons.php:163
msgid "Restore"
msgstr "Restaurar"

#: includes/admin-addons.php:139 includes/admin-addons.php:173
msgid "Delete"
msgstr "Borrar"

#: includes/admin-screens.php:269 includes/admin-addons.php:138
msgid "Author"
msgstr "Autor"

#: includes/admin-addons.php:137
msgid "Revision"
msgstr "Revisión"

#: includes/admin-addons.php:136 includes/admin-addons.php:169
msgid "Compare"
msgstr "Comparar"

#: includes/admin-addons.php:114
msgctxt "revision date format"
msgid "F j, Y @ H:i:s"
msgstr "j \\d\\e F \\d\\e Y @ G:i"

#: includes/admin-addons.php:103
msgid "Add"
msgstr "Añadir"

#: includes/admin-addons.php:102
msgid "Text filter"
msgstr "Filtrar texto"

#: includes/admin-addons.php:100 includes/admin-addons.php:101
msgid "URL"
msgstr "URL"

#: includes/admin-addons.php:89
msgid "Ends by"
msgstr "Termina con"

#: includes/admin-addons.php:85
msgid "Not contains"
msgstr "No contiene"

#: includes/admin-addons.php:84
msgid "Contains"
msgstr "Contiene"

#: includes/admin-addons.php:82
msgid "All Website"
msgstr "Toda la web"

#: includes/admin-addons.php:68
msgid "Preview Changes"
msgstr "Previsualizar cambios"

#: includes/admin-screens.php:763 includes/admin-addons.php:42
msgid "Available only in <br />Simple Custom CSS and JS Pro"
msgstr "Disponible solo en <br />Simple Custom CSS and JS Pro"

#: includes/admin-addons.php:67
msgid "Full URL on which to preview the changes ..."
msgstr "URL completa en la que previsualizar los cambios…"

#: includes/admin-addons.php:56
msgid "Code Revisions"
msgstr "Revisiones de código"

#: includes/admin-addons.php:55
msgid "Apply only on these URLs"
msgstr "Aplicar solo en estas URLs"

#: includes/admin-addons.php:54
msgid "Preview"
msgstr "Previsualizar"

#: custom-css-js.php:263 includes/admin-config.php:58
msgid "Settings"
msgstr "Ajustes"

#: includes/admin-install.php:107
msgid "Web Designer"
msgstr "Diseñador web"

#: includes/admin-install.php:62
msgid "Custom CSS and JS code"
msgstr "Código CSS y JS personalizado"

#: includes/admin-install.php:41
msgid "No Custom Code found in Trash."
msgstr "No se ha encontrado código personalizado en la papelera."

#: includes/admin-install.php:40
msgid "No Custom Code found."
msgstr "No se ha encontrado código personalizado."

#: includes/admin-install.php:39
msgid "Parent Custom Code:"
msgstr "Código personalizado superior:"

#: includes/admin-install.php:38
msgid "Search Custom Code"
msgstr "Buscar código personalizado"

#: includes/admin-install.php:37
msgid "All Custom Code"
msgstr "Todo el código personalizado"

#: includes/admin-install.php:36
msgid "View Custom Code"
msgstr "Ver código personalizado"

#: includes/admin-install.php:35
msgid "Edit Custom Code"
msgstr "Editar código personalizado"

#: includes/admin-install.php:31
msgctxt "add new on admin bar"
msgid "Custom Code"
msgstr "Código personalizado"

#: includes/admin-install.php:30
msgctxt "admin menu"
msgid "Custom CSS & JS"
msgstr "CSS y JS personalizado"

#: includes/admin-install.php:29
msgctxt "post type singular name"
msgid "Custom Code"
msgstr "Código personalizado"

#: includes/admin-install.php:28
msgctxt "post type general name"
msgid "Custom Code"
msgstr "Código personalizado"

#: includes/admin-screens.php:808 includes/admin-screens.php:896
msgid "Where in site"
msgstr "Ubicación en el sitio"

#: includes/admin-screens.php:793 includes/admin-screens.php:881
msgid "Where on page"
msgstr "Ubicación en la página"

#: includes/admin-screens.php:647
msgid ""
"/* Add your CSS code here.\n"
"\n"
"For example:\n"
".example {\n"
"    color: red;\n"
"}\n"
"\n"
"For brushing up on your CSS knowledge, check out http://www.w3schools.com/css/css_syntax.asp\n"
"\n"
"End of comment */ "
msgstr ""
"/* Añade aquí tu código CSS.\n"
"\n"
"Por ejemplo:\n"
".ejemplo {\n"
"    color: red;\n"
"}\n"
"\n"
"Para mejorar tu conocimiento sobre CSS knowledge echa un vistazo a http://www.w3schools.com/css/css_syntax.asp\n"
"\n"
"Fin del comentario */ "

#: includes/admin-warnings.php:58
msgid "Please remove the <b>custom-css-js</b> post type from the <b>qTranslate settings</b> in order to avoid some malfunctions in the Simple Custom CSS & JS plugin. Check out <a href=\"%s\" target=\"_blank\">this screenshot</a> for more details on how to do that."
msgstr "Por favor, elimina el tipo de contenido <b>custom-css-js</b> de los <b>ajustes de qTranslate</b> para evitar algunos errores en el plugin Simple Custom CSS & JS. Echa un vistazo a <a href=\"%s\" target=\"_blank\">esta captura de pantalla</a> para más detalles sobre cómo hacerlo."

#: includes/admin-screens.php:621
msgid ""
"<!-- Add HTML code to the header or the footer.\n"
"\n"
"For example, you can use the following code for loading the jQuery library from Google CDN:\n"
"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\n"
"\n"
"or the following one for loading the Bootstrap library from MaxCDN:\n"
"<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n"
"\n"
"-- End of the comment --> "
msgstr ""
"<!-- Añade código HTML a la cabecera o pie de página.\n"
"\n"
"Por ejemplo, puedes usar el siguiente código para cargar la biblioteca jQuery desde la CDN de Google:\n"
"<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js\"></script>\n"
"\n"
"o el siguiente para cargar la biblioteca Bootstrap desde MaxCDN:\n"
"<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">\n"
"\n"
"-- Fin del comentario --> "