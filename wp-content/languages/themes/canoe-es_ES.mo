��          D      l       �      �   �  �           *    E     G    M     f     u                          Canoe HTML5 & CSS3 Responsive WordPress Business theme with business style home page layout with welcome section, 3 product/services blocks and a client quote/testimonial section. 2 logo section layout options. 2 premade (Blue, Red) ready to use color schemes/skins. 3 widget areas in footer, 1 widget area in sidebar. 2 page layouts including a full width page template. Social media icons in footer. ThemeAlley.com http://www.themealley.com/ PO-Revision-Date: 2018-11-08 20:36:14+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/2.4.0-alpha
Language: es
Project-Id-Version: Themes - Canoe
 Canoe Tema de negocio adaptable para WordPress basado en HTML5 & CSS3 con diseño de página de inicio de estilo empresarial con sección de bienvenida, 3 bloques de productos y servicios y una sección de testimonios de clientes. 2 opciones de diseño de la sección de logotipos. 2 pieles prefabricadas (Azul, Rojo) listas para usar. 3 áreas de widgets en el pie de página, 1 área de widgets en la barra lateral. 2 diseños de página incluyendo una plantilla de página de ancho completo. Iconos de medios sociales en el pie de página. ThemeAlley.com http://www.themealley.com/ 